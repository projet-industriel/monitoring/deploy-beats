
>_Date:_ Lundi 21 mars 2023.
>
> **Mastère Spécialisé - Infrastructure Cloud et  DevOps 2022-23**
>
> GROUPE 3
>
# Monitoring et logging du microservice meteo-icd

## Présentation

Ce dépôt est destiné à la configuration des agents de collecte de métriques et de log dans un cluster kubernetes respectivement avec `metricbeat` et `filebeat`. 
Chacun de ces deux agents fonctionnent avec des `modules` et sont executés dans des pods de `DaemonSet` sur tous les noeuds du cluster kubernetes.

Des volumes de types `configMap` sont utilisés pour l'accès aux fichiers de log. Un fichier de manifest est mis à disposition pour le déploiement des objets liés aux `DaemonSet`.

## Pré-requis:

Les instructions contenues dans ce dépôt font suite au projet de déploiement de l'application meteo-icd du même groupe de projets que celui-ci.

On suppose que le cluster d'orchestration est kubernetes et héberge déja l'application meteo-icd.

Les contraintes suivantes sont nécessaires:

Les `taints` du controlplane doivent correspondre aux tolérances suivantes:
```sh
       tolerations:
       - effect: "NoExecute"
         operator: "Exists"
       - effect: "NoSchedule"
         operator: "Exists"
```

 Le gestionnaire de paquetage `helm` doit être installé sur le noeud de controlplane. Cette nécessité est satisfaite par le playbook `environment.yml`.

## Meticbeat
Les caractéristiques principales de cet agent sont:
- [x] effectue des mesures de statistiques telles que celles des systèmes d'hôte, de docker et de service/processus ainsi que celles des evenements kubernetes;
- [x] utilise des modules standards ou personalisés;
- [x] fournit des information relatives au `cgroups` à partir du système de fichier `proc` (ref. [doc](https://www.elastic.co/fr/beats/metricbeat));
- [x]  dépend de [kubernetes-state-metrics](https://www.elastic.co/guide/en/beats/metricbeat/current/running-on-kubernetes.html);
- [x] utilise le sous-système [autodiscover](https://www.elastic.co/guide/en/beats/metricbeat/current/configuration-autodiscover.html) monitorer les services en cours d'exécution.



## Filebeat

Tout comme metricbeat, il effectue la collecte à l'aide des modules et les inputs configurés suivants la localisation des fichiers de log. Il récupère ainsi tous les flux de logs, avec leur pod, leur conteneur, leur nœud, leur VM, leur hôte et leurs autres métadonnées pour une corrélation automatique. Les fonctionnalités Autodiscover des agents Beats détectent les nouveaux conteneurs et les surveillent de manière évolutive avec les modules Filebeat adaptés. 

## Déploiement 
Les étapes indiquées à travers ce [lien](http://elatov.github.io/2020/01/monitoring-kubernetes-with-metricbeat/#deploying-kube-state-metrics) sont les références pour résoudre la dépendance de `kubernetes-state-metrics`. Une copie  du [dépot](https://github.com/kubernetes/kube-state-metrics.git) est disponible dans ce projet et les étapes à suivre sont automatisees par la chaine de deploiement de ce depot.


* La chaîne de déploiement de ce dépot résoud les dépendances évoqées et déploie automatiquement les agents dans le cluster kubernetes.

* Les variables de `pipeline` importantes  pour le déploiement sont contenues dans le fichier de variable Gitlab `METEO_PROD_VARS` notamment `METEO_VOLUME_PATH`, `NGINX_ACCESS_LOG_PATHS` et `NGINX_ERROR_LOG_PATHS`.

* Egalement notez les variables de groupe de projets :
  * `NODE_PUBLIC_IP` qui correspond a l'adresse IP de `Elasticsearch`;
  * `ELASTICSEARCH_PORT` pour le port de `Elasticsearch`;
  * `KIBANA_PORT` pour le port de `Kibana`;

* Le déclenchement de la chaîne est assuré par des contraintes de branches protégées `master` et `*-release` lors des commit; ou manuellement en sélectionnant un `tag` correspondant a un numéro de release puis l'exécution avec le bouton `Run pipeline` (menu CI/CD > Pipelines). 


>_NB:_ 
> À chaque numéro de `release` correspond un déploiement spécifique suivant les changements désirés par l'itération.
>

Pour vérifier que la dépendance `kube-state-metrics` est operationnelles, il suffit de le faire  avec un port-forward:

```sh
kubectl port-forward svc/kube-state-metrics -n kube-system 9000:8080 --address 0.0.0.0
```
Visiter le lien http://@IP_NODES:9000/metrics avec le navigateur ou faire :
```sh
curl http://@ClusterIP:8080/metrics -s | grep deployment_status_replicas_availab
```

Une fois les agents deployés les métriques et log sont remontés à la plateforme d'observabilité `EK`.

